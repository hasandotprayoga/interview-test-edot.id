# INTERVIEW TEST
Hasan Prayoga, Bandung 2022-01-26  
Original Private Repo: [https://github.com/hasandotprayoga/interview-test](https://github.com/hasandotprayoga/interview-test)  
## LOGIC
1. If a clock strikes once at 1 o′clock, twice at 2 o′clock and so on, How many times will it strike in a day?
> The clock Wil strike **156** times in 24 hours. It's 1+2+3+4+5+6+7+8+9+10+11+12 = 78 times in 12 hours. For 24 hours it's double 156 times.
2. A snail is at the bottom of a 30 foot well. Every hour the snail is able to climb up 3 feet, then immediately slide back down 2 feet. How many hours does it take for the snail to get out of the well?
> Snails need **28 hours** to get out of the well. Because when it reaches 27 feet, the snail climbs 3 feet and straight out of the well.
3. I live on Sunset Boulevard, where there are 6 houses on my side of the block. The house numbers are consecutive even numbers. The sum of all 6 house numbers is 8790. You don't know which block I live on, and it's a long street, but I will tell you that I live in the lowest number on my side of the block. What's my address?
> The total of all house numbers is 8790, so 8790 / 6 = 1465 for the average of the sum of 6 house numbers. So there must be 3 house numbers that are less than that number and 3 house numbers that are more than that. All the numbers are even, so they can be sorted as follows: 1460, 1462, 1464, 1466, 1468, 1470. He says he lives in the lowest number on his side of the block. So 1460 which is the lowest number is his house number. **So his address is at Sunset Boulevard number 1460**.
4. A man needs to cross a river in a canoe. With him, he has a bag of grain, a chicken, and a fox. He can only carry one of the three at a time. If he leaves the grain and the chicken, the chicken will eat the grain. If he takes the grain, the fox will eat the chicken. How does he successfully cross the river with his load?
> Had thought to let the chicken eat the grain and the fox eaten the chicken, then just take it across the fox.
But that's not the right answer, I'm just kidding :D  
So the man had to take the chicken across the river first.
Then brought the fox across the silence and the chicken was brought back.
Chickens are kept first and carry grain across the river.
And finally brought across the chicken again.

## MySQL
1. what is the difference between InnoDB and MyISAM?
> In theory I'm not very deep into this, but after doing some research via Google here are my conclusions:

- MyISAM does not support transactions by tables while InnoDB supports.
- There are no possibility of row-level locking, relational integrity MyISAM but with InnoDB this is possible. MyISAM has table-level locking.
- InnoDB does not support FULLTEXT index while MyISAM supports.
- Performance speed of MyISAM table is much higher as compared with tables in InnoDB.
- InnoDB is better option while you are dealing with larger database because it supports transactions, volume while MyISAM is suitable for small project.
- As InnoDB supports row-level locking which means inserting and updating is much faster as compared with MyISAM.
- InnoDB supports ACID (Atomicity, Consistency, Isolation and Durability) properly while MyISAM does not support.
- In InnoDB table, AUTO_INCREMENT field is part of index.
- Once table in InnnoDB is deleted then it can not re-establish.
- InnoDB does not save data as table level so while implementation of select count(*) from table will again scan the whole table to calculate the number of rows while MyISAM save data as table level so you can easily read out the saved row number.
- MyISAM does not support FOREIGN-KEY referential-integrity constraints while InnoDB supports.

2. List out the employees annual salary with their names only.
```sql
SELECT
	CONCAT(LAST_NAME, ' ', COALESCE(MIDDLE_NAME, ''), ' ', COALESCE(LAST_NAME,'')) AS EMPLOYEE_NAME
	, SALARY
FROM EMPLOYEE;
```
3. List out the employees who are working in department 10 and draw the salaries more than 3500.
```sql
SELECT
	*
FROM EMPLOYEE 
WHERE 
	DEPARTMENT_ID = 10
	AND SALARY > 3500;
```
4. List out the department name, maximum salary, minimum salary, average salary of the employees.
```sql
SELECT
	d.Name AS DEPARTMENT_NAME
	, MAX(e.SALARY) AS MAXIMUM_SALARY
	, MIN(e.SALARY) AS MINIMUM_SALARY
	, AVG(e.SALARY) AS AVERAGE_SALARY
FROM EMPLOYEE AS e
INNER JOIN DEPARTMENT AS d ON d.Department_ID = e.DEPARTMENT_ID
GROUP BY d.Department_ID;
```
5. Display all employees in sales or operation departments.
```sql
SELECT
	e.*
FROM EMPLOYEE AS e 
INNER JOIN DEPARTMENT AS d ON d.Department_ID = e.DEPARTMENT_ID
WHERE d.Name IN ('SALES', 'OPERATIONS');
```
## PHP/JS/Golang
> I understand PHP and Javascript, but in this case this i choose PHP.

1. Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz”.

Solving;
```php
<?php
$results = [];

for ($i = 1; $i <= 100; $i++) {
    if ($i % 15 == 0) {
        $results[] = 'FizzBuzz';
    } else if ($i % 3 == 0) {
        $results[] = 'Fizz';
    } else if ($i % 5 == 0) {
        $results[] = 'Buzz';
    } else {
        $results[] = $i;
    }
}

echo implode(PHP_EOL, $results);

```

2. Create a function to receive an integer input from user which translates to string currency.
Example:
```php
function convert($input) { // 123456
    return $output; // one hundred twenty three thousand four hundred fifty six
}
```

Solving;
```php
<?php 
function convertNumberTowords($num)
{
    $ones = [
        'zero',
        'one',
        'two',
        'three',
        'four',
        'five',
        'six',
        'seven',
        'eight',
        'nine',
        'ten',
        'eleven',
        'twelve',
        'thirteen',
        'fourteen',
        'fifteen',
        'sixteen',
        'seventeen',
        'eighteen',
        'nineteen'
    ];

    $tens = [
        'zero',
        'ten',
        'twenty',
        'thirty',
        'forty',
        'fifty',
        'sixty',
        'seventy',
        'eighty',
        'ninety'
    ];

    $hundreds = [
        'hundred',
        'thousand',
        'million',
        'billion',
        'trillion',
        'quardrillion'
    ];

    $num = number_format($num, 2, '.', ',');
    $numArr = explode('.', $num);

    $intNum = $numArr[0];
    $decNum = $numArr[1];

    $intNumArr = array_reverse(explode(',', $intNum));

    krsort($intNumArr, 1);

    $words = '';
    foreach ($intNumArr as $key => $i) {

        while (substr($i, 0, 1) == '0')
            $i = substr($i, 1, 5);
        if ($i < 20) {
            $words .= $ones[$i];
        } elseif ($i < 100) {
            if (substr($i, 0, 1) != '0')  $words .= $tens[substr($i, 0, 1)];
            if (substr($i, 1, 1) != '0') $words .= ' ' . $ones[substr($i, 1, 1)];
        } else {
            if (substr($i, 0, 1) != '0') $words .= $ones[substr($i, 0, 1)] . ' ' . $hundreds[0];
            if (substr($i, 1, 1) != '0') $words .= ' ' . $tens[substr($i, 1, 1)];
            if (substr($i, 2, 1) != '0') $words .= ' ' . $ones[substr($i, 2, 1)];
        }
        if ($key > 0) {
            $words .= ' ' . $hundreds[$key] . ' ';
        }
    }
    if ($decNum > 0) {
        $words .= ' and ';
        if ($decNum < 20) {
            $words .= $ones[$decNum];
        } elseif ($decNum < 100) {
            $words .= $tens[substr($decNum, 0, 1)];
            $words .= ' ' . $ones[substr($decNum, 1, 1)];
        }
    }

    return $words;
}

echo convertNumberTowords(123456);
```

3. Create a multiply function without * operator.
Example:
```php
function multiply($a, $b) { // a = 5, b = 4
    return $result; // 20
}
```

Solving:
```php
<?php
function multiply($a, $b)
{
    if ($b == 0)
        return 0;

    if ($b > 0)
        return ($a + multiply($a, $b - 1));

    if ($b < 0)
        return -multiply($a, -$b);
}

echo multiply(5, 4);
```

4. Create a function to create a fibonacci number.
Example:
```php
function fibonacci($input) { // 10
    return $result; // 0, 1, 1, 2, 3, 5, 8, 13, 21, 34
}
```

Solving:

```php
<?php
function fibonacci($value)
{
    $numbers = [];
    $n1 = 0;
    $n2 = 1;

    $counter = 0;
    while ($counter < $value) {
        if (!is_null($value)) {
            $numbers[] = $n1;
        }
        $n3 = $n2 + $n1;
        $n1 = $n2;
        $n2 = $n3;
        $counter = $counter + 1;
    }

    return implode(', ', $numbers);
}

echo fibonacci(10);
```
<?php
$results = [];

for ($i = 1; $i <= 100; $i++) {
    if ($i % 15 == 0) {
        $results[] = 'FizzBuzz';
    } else if ($i % 3 == 0) {
        $results[] = 'Fizz';
    } else if ($i % 5 == 0) {
        $results[] = 'Buzz';
    } else {
        $results[] = $i;
    }
}

echo implode(PHP_EOL, $results);

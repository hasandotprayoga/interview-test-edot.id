<?php
function fibonacci($value)
{
    $numbers = [];
    $n1 = 0;
    $n2 = 1;

    $counter = 0;
    while ($counter < $value) {
        if (!is_null($value)) {
            $numbers[] = $n1;
        }
        $n3 = $n2 + $n1;
        $n1 = $n2;
        $n2 = $n3;
        $counter = $counter + 1;
    }

    return implode(', ', $numbers);
}

echo fibonacci(10);

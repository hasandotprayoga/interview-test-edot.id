<?php
function convertNumberTowords($num)
{
    $ones = [
        'zero',
        'one',
        'two',
        'three',
        'four',
        'five',
        'six',
        'seven',
        'eight',
        'nine',
        'ten',
        'eleven',
        'twelve',
        'thirteen',
        'fourteen',
        'fifteen',
        'sixteen',
        'seventeen',
        'eighteen',
        'nineteen'
    ];

    $tens = [
        'zero',
        'ten',
        'twenty',
        'thirty',
        'forty',
        'fifty',
        'sixty',
        'seventy',
        'eighty',
        'ninety'
    ];

    $hundreds = [
        'hundred',
        'thousand',
        'million',
        'billion',
        'trillion',
        'quardrillion'
    ];

    $num = number_format($num, 2, '.', ',');
    $numArr = explode('.', $num);

    $intNum = $numArr[0];
    $decNum = $numArr[1];

    $intNumArr = array_reverse(explode(',', $intNum));

    krsort($intNumArr, 1);

    $words = '';
    foreach ($intNumArr as $key => $i) {

        while (substr($i, 0, 1) == '0')
            $i = substr($i, 1, 5);
        if ($i < 20) {
            $words .= $ones[$i];
        } elseif ($i < 100) {
            if (substr($i, 0, 1) != '0')  $words .= $tens[substr($i, 0, 1)];
            if (substr($i, 1, 1) != '0') $words .= ' ' . $ones[substr($i, 1, 1)];
        } else {
            if (substr($i, 0, 1) != '0') $words .= $ones[substr($i, 0, 1)] . ' ' . $hundreds[0];
            if (substr($i, 1, 1) != '0') $words .= ' ' . $tens[substr($i, 1, 1)];
            if (substr($i, 2, 1) != '0') $words .= ' ' . $ones[substr($i, 2, 1)];
        }
        if ($key > 0) {
            $words .= ' ' . $hundreds[$key] . ' ';
        }
    }
    if ($decNum > 0) {
        $words .= ' and ';
        if ($decNum < 20) {
            $words .= $ones[$decNum];
        } elseif ($decNum < 100) {
            $words .= $tens[substr($decNum, 0, 1)];
            $words .= ' ' . $ones[substr($decNum, 1, 1)];
        }
    }

    return $words;
}

echo convertNumberTowords(123456);
